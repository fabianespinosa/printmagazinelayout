# Excercise Print Magazines Layout

Comming from here: [Influencing Web Layouts with Print Layouts](https://css-tricks.com/print-magazine-layouts-converted-to-web-layouts/)

Live @ [f-spin.net](https://f-spin.net/PrintMagazineLayout/)

*Inspired by*: Vertigo.

![foto example](https://bytebucket.org/fabianespinosa/printmagazinelayout/raw/0ded970b604ec3de6dae25532fd9f36048b6cced/example/vertigo.jpg)
Titles & tekst: [Vertigo](http://vertigoweb.be/)

Modal made with **jQuery** & triggered when scrolling after `<blockquote>`.

## Dependencies:

* [stylus](http://stylus-lang.com/)
* [normalize.styl](https://github.com/bymathias/normalize.styl)

## Fonts

* Body [Arvo](https://www.google.com/fonts/specimen/Arvo)
* Title [Kanit](https://www.google.com/fonts/specimen/Kanit)
* Quotes [BenchNine](https://www.google.com/fonts/specimen/BenchNine)

### Resources

* [Fillerati](http://www.fillerati.com/) - Alt Lorem Ipsum
* [The inherit, initial, and unset values](http://www.quirksmode.org/css/cascading/values.html)
* [Tricky CSS conditional sibling > child selector](http://stackoverflow.com/a/3833068/1964399)
* [The Shapes of CSS - CSS Tricks](https://css-tricks.com/examples/ShapesOfCSS/)